variables:
  GIT_DEPTH: 1
  GIT_STRATEGY: none
  GIT_SUBMODULE_STRATEGY: none
  PRECICE_VERSION: "3.1.1"
  OPENFOAM_VERSION: "2312"
  CODENAME: "noble"
  ADAPTER_VERSION: "1.3.0"
  TUTORIALS_REPO: "https://github.com/precice/tutorials.git"
  TUTORIALS_REPO_BRANCH: "v202404.0"
  DEB_PRECICE: "https://github.com/precice/precice/releases/download/v${PRECICE_VERSION}/libprecice3_${PRECICE_VERSION}_${CODENAME}.deb"
  DEB_PRECICE_SHA: "https://github.com/precice/precice/releases/download/v${PRECICE_VERSION}/libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256"

default:
  image: ubuntu:${CODENAME}

build-libprecice3:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb >/dev/null 2>&1
    - apt-cache show libprecice3
    - dpkg -L libprecice3

build-pyprecice:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256 >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb python3-pip python3-venv python3-mpi4py libopenmpi-dev pkg-config >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y --reinstall build-essential >/dev/null 2>&1
    - python3 -m venv --system-site-packages ~/pyprecice-env && source ~/pyprecice-env/bin/activate && pip -v --log pyprecice.log install pyprecice
    - pip list && pip show pyprecice && python -c 'import precice;'
    - cat ~/pyprecice-env/lib/python3.12/site-packages/precice/_version.py ~/pyprecice-env/lib/python3.12/site-packages/precice/__init__.py
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/*.log

build-openfoam:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl https://dl.openfoam.com/add-debian-repo.sh | bash >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends --no-install-suggests -y openfoam${OPENFOAM_VERSION}-default >/dev/null 2>&1
    - apt-cache show openfoam${OPENFOAM_VERSION} openfoam${OPENFOAM_VERSION}-common openfoam${OPENFOAM_VERSION}-default openfoam${OPENFOAM_VERSION}-dev openfoam${OPENFOAM_VERSION}-source openfoam${OPENFOAM_VERSION}-tools openfoam${OPENFOAM_VERSION}-tutorials
    - dpkg -L openfoam${OPENFOAM_VERSION} openfoam${OPENFOAM_VERSION}-common openfoam${OPENFOAM_VERSION}-default openfoam${OPENFOAM_VERSION}-dev openfoam${OPENFOAM_VERSION}-source #openfoam${OPENFOAM_VERSION}-tools openfoam${OPENFOAM_VERSION}-tutorials
    - cat /usr/lib/openfoam/openfoam${OPENFOAM_VERSION}/etc/bashrc
    - apt-get install --no-install-recommends --no-install-suggests -y openfoam-selector >/dev/null 2>&1
    - apt-cache show openfoam-selector
    - dpkg -L openfoam-selector

build-openfoam-precice:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256 >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb >/dev/null 2>&1
    - curl https://dl.openfoam.com/add-debian-repo.sh | bash >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends --no-install-suggests -y openfoam${OPENFOAM_VERSION}-default >/dev/null 2>&1
    - curl -LO "https://github.com/precice/openfoam-adapter/releases/download/v${ADAPTER_VERSION}/openfoam-adapter-${ADAPTER_VERSION}-OpenFOAMv1812-v${OPENFOAM_VERSION}-newer.tar.gz"
    - tar -xvf openfoam-adapter-${ADAPTER_VERSION}-OpenFOAMv1812-v${OPENFOAM_VERSION}-newer.tar.gz
    - cd openfoam-adapter-${ADAPTER_VERSION}
    - source /usr/lib/openfoam/openfoam${OPENFOAM_VERSION}/etc/bashrc || true
    - apt-get install --no-install-recommends --no-install-suggests -y pkg-config >/dev/null 2>&1
    - ./Allclean && ./Allwmake && mv *.log ${CI_PROJECT_DIR} >/dev/null 2>&1
    - ls -l lnInclude
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/*.log

check_precice-tools_stable:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256 >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y git >/dev/null 2>&1
    - git clone -q --filter=blob:none --depth=1 --branch ${TUTORIALS_REPO_BRANCH} ${TUTORIALS_REPO}
    - pushd tutorials/quickstart/ && precice-tools check precice-config.xml && popd || true
    - pushd tutorials/perpendicular-flap/ && precice-tools check precice-config.xml && popd || true

check_precice-tools_unstable:
  stage: build
  variables:
    TUTORIALS_REPO_BRANCH: "master"
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256 >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y git >/dev/null 2>&1
    - git clone -q --filter=blob:none --depth=1 --branch ${TUTORIALS_REPO_BRANCH} ${TUTORIALS_REPO}
    - pushd tutorials/quickstart/ && precice-tools check precice-config.xml && popd
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/*.log

test_solverdummy:
  stage: build
  script:
    - apt-get update >/dev/null 2>&1 && DEBIAN_FRONTEND="noninteractive" apt-get -y dist-upgrade >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y ca-certificates curl >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE} >/dev/null 2>&1
    - curl -LO ${DEB_PRECICE_SHA} >/dev/null 2>&1
    - sha256sum -c libprecice3_${PRECICE_VERSION}_${CODENAME}.deb.sha256 >/dev/null 2>&1
    - DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends --no-install-suggests -y ./libprecice3_${PRECICE_VERSION}_${CODENAME}.deb cmake >/dev/null 2>&1
    - apt-get install --no-install-recommends --no-install-suggests -y --reinstall build-essential >/dev/null 2>&1
    - cp -R /usr/share/precice/examples/solverdummies .
    - cmake -S solverdummies/c -B build-c
    - cmake -S solverdummies/cpp -B build-cpp
    - cmake -S solverdummies/fortran -B build-fortran
    - precice-tools check solverdummies/precice-config.xml
